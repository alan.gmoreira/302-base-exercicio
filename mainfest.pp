$svc_conf_template = @(END)
[Unit]
Description=Api de investimentos

[Service]
ExecStart=/usr/bin/java -jar /home/ubuntu/Api-Investimentos-0.0.1-SNAPSHOT.jar

[Install]
WantedBy=multi-user.target
END

# instalar o java, especificamente pacote 'openjdk-8-jdk'

class { 'java' :
  package => 'openjdk-8-jdk',
}
# exemplo na documentação do módulo

file { '/etc/systemd/system/Api-Investimentos.service':
ensure => file,
content => inline_epp($svc_conf_template),
}

service{"Api-Investimentos":
ensure => stopped,
}

# gerenciar o tipo de recurso correspondente ao service acima e garantir que está rodando

# configurar o nginx (instalação e proxy reverso)
# (tem o exemplo na documentação do módulo no forge.puppet.com)
include nginx

nginx::resource::server { 'Api-investimento':
  listen_port => 80,
  proxy       => 'http://localhost:5000',
}

file { "/etc/nginx/conf.d/default.conf":
ensure => absent,
notify => Service['nginx'],
}

ssh_authorized_key { "jemkins@banana":
	ensure => present,
	user => "ubuntu",
	type=> "ssh-rsa",
	key => "AAAAB3NzaC1yc2EAAAADAQABAAABAQDvNfe5q8SfXeGyl6cB2vR4mo7pHq30sjPgMrhP05UF2gH3bgz7dGREpb6caCGJo8gpQXofeEI0wdqLw0KbmIuqSMpT4yQQHWr6VqWbDaK2XQutHJkElGQMTCBADwT4bGt8b2eNV8s581g/RS01q4L7F9iSkMzSms3BM63FkZHZAOlOTHmRnN3Ju39MolmjGPwb6SpUFYpMIZLic1vKTe4p1L3PQL4czIJxpssE283iOeRiAceD/Xaly4mgVqdxmMEm3gZo5PzcPMu5Y08M6Ur0vJR5i4IVSSheBhzcKLshuvTBt5bobYtG1BWatQIpbKoCO6Uqnf/7U7FvPHJTYsJb",
}

ssh_authorized_key { "a2w@localhost.localdomain":
        ensure => present,
        user => "ubuntu",
        type=> "ssh-rsa",
	key => "AAAAB3NzaC1yc2EAAAADAQABAAABgQDMaI7RX73lJEc0HoBswZNE6MOqz7hIlQ+PqUXFttl3kfcThDvRsVAuPhSsjm6XG9fWhYkpDVpn7cc88AXMW3b/9105eticsC1gjyWIkSVzBUtdOBByDTd4t4yR+iwbewpXCS72POszpOVp36p75EX4RvCW9EVNVLkaCDUHz352mTKdkh5i2UshWvitR+Qkwdc4wlFrtAW5Tdtaa1l+w3d++gNX2dvxGz/3qnlQe0oTYaSclvM2aSsVaIh49apidtV63FMv4D8qQVsfCMuB77fdpil+2S53xJF/zzN/FZG+K/dFp5flHja6aqNy/uSjyI97xQYiERAzLXCfHQnSfJzlhwjI36K3QKRqs4uBeggzbfUyi7wMIWuethbnkeTdP+RKSDw5WLV9lbCR3xTNhvLjI0FBHHRbdM6NuYjCfEwTX9MoMZGz6CMHZ6hfoQsVKGfE//s0cHO/y5zsxN+X6sgiWEWALkhYIutCwxS389AounJ8WY16QanNm029Maq6Uis="
}