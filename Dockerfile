FROM openjdk:8-jre-alpine
COPY ./target/Api-Investimentos-0.0.1-SNAPSHOT.jar /app/Api-Investimento/
CMD ["java", "-jar", "/app/Api-Investimento/Api-Investimentos-0.0.1-SNAPSHOT.jar"]